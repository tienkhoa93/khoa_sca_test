IF OBJECT_ID('[dbo].[sp_test]') IS NOT NULL
	DROP PROCEDURE [dbo].[sp_test];

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_test]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT 'SQL Change Automation' AS Title,'Khoa' AS Test
END
GO
