CREATE TABLE [dbo].[Student]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) NULL
)
GO
ALTER TABLE [dbo].[Student] ADD CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED  ([ID])
GO
