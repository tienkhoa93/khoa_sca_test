﻿-- <Migration ID="4bb2dbb6-1b18-4bba-a3db-fb3398110c7d" />
GO

PRINT N'Creating [dbo].[Student]'
GO
CREATE TABLE [dbo].[Student]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) NULL
)
GO
PRINT N'Creating primary key [PK_Student] on [dbo].[Student]'
GO
ALTER TABLE [dbo].[Student] ADD CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED  ([ID])
GO
